package com.mt.error.dal.common.mapper;

import com.mt.error.dal.common.param.MapperQuery;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yuwenzhong on 2017/01/04.
 */
public interface BaseMapper< T extends Serializable, ID extends Serializable> {
    long insert(T entity);

    void insertBatch(List<T> entityList);

    int update(T entity);

    T findOne(ID id);

    List<T> findAll();

    long count();

    List<T> findByIds(Iterable<ID> ids);

    List<T> findList(MapperQuery mapperQuery);

    long countList(MapperQuery mapperQuery);

    int delete(ID id);

    int deleteByIds(Iterable<ID> ids);
}
