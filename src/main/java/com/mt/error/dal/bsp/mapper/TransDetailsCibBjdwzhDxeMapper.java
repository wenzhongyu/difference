package com.mt.error.dal.bsp.mapper;

import com.mt.error.dal.bsp.entity.TransDetailsCibBjdwzhDxeDO;
import com.mt.error.dal.common.mapper.BaseMapper;

/**
 * Created by wenzhong on 2017/1/4.
 */
public interface TransDetailsCibBjdwzhDxeMapper extends BaseMapper<TransDetailsCibBjdwzhDxeDO, Long>{
}
