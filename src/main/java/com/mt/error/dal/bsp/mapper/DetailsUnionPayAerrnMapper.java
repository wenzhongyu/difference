package com.mt.error.dal.bsp.mapper;

import com.mt.error.dal.bsp.entity.DetailsUnionPayAerrnDO;
import com.mt.error.dal.common.mapper.BaseMapper;

/**
 * Created by wenzhong on 2017/1/4.
 */
public interface DetailsUnionPayAerrnMapper extends BaseMapper<DetailsUnionPayAerrnDO, Long> {
}
