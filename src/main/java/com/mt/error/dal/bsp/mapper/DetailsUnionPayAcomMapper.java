package com.mt.error.dal.bsp.mapper;

import com.mt.error.dal.bsp.entity.DetailsUnionPayAcomDO;
import com.mt.error.dal.common.mapper.BaseMapper;

/**
 * Created by wenzhong on 2017/1/4.
 */
public interface DetailsUnionPayAcomMapper extends BaseMapper<DetailsUnionPayAcomDO, Long> {
}
