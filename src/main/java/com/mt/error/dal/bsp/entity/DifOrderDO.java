package com.mt.error.dal.bsp.entity;

import com.alibaba.fastjson.JSONObject;
import com.mt.error.dal.common.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wenzhong on 2017/1/4.
 */
@Entity
@Table(name = "diforder")
public class DifOrderDO extends BaseEntity {

    private String gid;                     //差异GID
    private Date collect_time;              //汇总时间
    private String bank_name;               //银行名称
    private String in_orout;                //出入金标识
    private String finance_order_id;        //财务流水号
    private String system_order_id;         //系统订单号
    private String bank_order_id;           //银行订单号
    private String user_no;                 //用户编号
    private BigDecimal pay_money;           //交易金额
    private BigDecimal final_fee;           //手续费
    private Date pay_finish_time;           //
    private Date clear_date;                //清算日期
    private String txn_type;                //交易类型
    private String biz_type;                //业务类型
    private String from_channel;            //来源渠道
    private String deal_channel;            //处理渠道
    private String dif_state;               //差异状态
    private String dif_type;                //差异类型
    private String deal_state;              //处理状态
    private Date deal_time;                 //处理时间
    private String deal_peo;                //处理人
    private String error_code;              //差异代码
    private String error_dec;               //差异描述
    private String check_mark;              //核对标识
    private Boolean ismix;                  //是否调账
    private String mix_type;                //调账类型
    private Date mix_finish_time;           //完成时间
    private String tz_state;                //调账状态
    private String sys_type;                //系统类型
    private String track_no;
    private String send_idcode;
    private String rec_idcode;
    private String trans_time;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public Date getCollect_time() {
        return collect_time;
    }

    public void setCollect_time(Date collect_time) {
        this.collect_time = collect_time;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getIn_orout() {
        return in_orout;
    }

    public void setIn_orout(String in_orout) {
        this.in_orout = in_orout;
    }

    public String getFinance_order_id() {
        return finance_order_id;
    }

    public void setFinance_order_id(String finance_order_id) {
        this.finance_order_id = finance_order_id;
    }

    public String getSystem_order_id() {
        return system_order_id;
    }

    public void setSystem_order_id(String system_order_id) {
        this.system_order_id = system_order_id;
    }

    public String getBank_order_id() {
        return bank_order_id;
    }

    public void setBank_order_id(String bank_order_id) {
        this.bank_order_id = bank_order_id;
    }

    public String getUser_no() {
        return user_no;
    }

    public void setUser_no(String user_no) {
        this.user_no = user_no;
    }

    public BigDecimal getPay_money() {
        return pay_money;
    }

    public void setPay_money(BigDecimal pay_money) {
        this.pay_money = pay_money;
    }

    public BigDecimal getFinal_fee() {
        return final_fee;
    }

    public void setFinal_fee(BigDecimal final_fee) {
        this.final_fee = final_fee;
    }

    public Date getPay_finish_time() {
        return pay_finish_time;
    }

    public void setPay_finish_time(Date pay_finish_time) {
        this.pay_finish_time = pay_finish_time;
    }

    public Date getClear_date() {
        return clear_date;
    }

    public void setClear_date(Date clear_date) {
        this.clear_date = clear_date;
    }

    public String getTxn_type() {
        return txn_type;
    }

    public void setTxn_type(String txn_type) {
        this.txn_type = txn_type;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getFrom_channel() {
        return from_channel;
    }

    public void setFrom_channel(String from_channel) {
        this.from_channel = from_channel;
    }

    public String getDeal_channel() {
        return deal_channel;
    }

    public void setDeal_channel(String deal_channel) {
        this.deal_channel = deal_channel;
    }

    public String getDif_state() {
        return dif_state;
    }

    public void setDif_state(String dif_state) {
        this.dif_state = dif_state;
    }

    public String getDif_type() {
        return dif_type;
    }

    public void setDif_type(String dif_type) {
        this.dif_type = dif_type;
    }

    public String getDeal_state() {
        return deal_state;
    }

    public void setDeal_state(String deal_state) {
        this.deal_state = deal_state;
    }

    public Date getDeal_time() {
        return deal_time;
    }

    public void setDeal_time(Date deal_time) {
        this.deal_time = deal_time;
    }

    public String getDeal_peo() {
        return deal_peo;
    }

    public void setDeal_peo(String deal_peo) {
        this.deal_peo = deal_peo;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_dec() {
        return error_dec;
    }

    public void setError_dec(String error_dec) {
        this.error_dec = error_dec;
    }

    public String getCheck_mark() {
        return check_mark;
    }

    public void setCheck_mark(String check_mark) {
        this.check_mark = check_mark;
    }

    public Boolean getIsmix() {
        return ismix;
    }

    public void setIsmix(Boolean ismix) {
        this.ismix = ismix;
    }

    public String getMix_type() {
        return mix_type;
    }

    public void setMix_type(String mix_type) {
        this.mix_type = mix_type;
    }

    public Date getMix_finish_time() {
        return mix_finish_time;
    }

    public void setMix_finish_time(Date mix_finish_time) {
        this.mix_finish_time = mix_finish_time;
    }

    public String getTz_state() {
        return tz_state;
    }

    public void setTz_state(String tz_state) {
        this.tz_state = tz_state;
    }

    public String getSys_type() {
        return sys_type;
    }

    public void setSys_type(String sys_type) {
        this.sys_type = sys_type;
    }

    public String getTrack_no() {
        return track_no;
    }

    public void setTrack_no(String track_no) {
        this.track_no = track_no;
    }

    public String getSend_idcode() {
        return send_idcode;
    }

    public void setSend_idcode(String send_idcode) {
        this.send_idcode = send_idcode;
    }

    public String getRec_idcode() {
        return rec_idcode;
    }

    public void setRec_idcode(String rec_idcode) {
        this.rec_idcode = rec_idcode;
    }

    public String getTrans_time() {
        return trans_time;
    }

    public void setTrans_time(String trans_time) {
        this.trans_time = trans_time;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
