package com.mt.error.dal.bsp.entity;

import com.alibaba.fastjson.JSONObject;
import com.mt.error.dal.common.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wenzhong on 2017/1/4.
 */
@Entity
@Table(name = "details_uniopay_aerrn")
public class DetailsUnionPayAerrnDO extends BaseEntity {

    private String error_trade_sign;                    //差错交易标志
    private String proxy_code;                          //代理机构标识码
    private String send_identifi_code;                  //发送机构标识码
    private String system_track_num;                    //系统跟踪号
    private Date trade_time;                            //交易传输时间
    private String bank_num;                            //主帐号
    private BigDecimal trade_money;                     //交易金额
    private String message_type;                        //报文类型
    private String trade_message_no;                    //交易类型码
    private String account_type;                        //商户类型
    private String terminal_yard;                       //受卡机终端标识码
    private String old_trade_retrieval_reference_num;   //上一笔交易检索参考号
    private String service_point_condetion;             //服务点条件码
    private String authorization_response_code;         //授权应答码
    private String acceptance_mechanism;                //接收机构标识码
    private String bank_identity_no;                    //发卡银行标识码
    private String old_trade_system_track_num;          //上一笔交易的系统跟踪号
    private String trade_return_code;                   //交易返回码
    private String service_input_style;                 //服务点输入方式
    private BigDecimal agency_hand_fee;                 //受理方应收手续费
    private BigDecimal agency_pay_fee;                  //受理方应付手续费
    private BigDecimal installment_payment_plus;        //分期付款附加手续费
    private BigDecimal bank_per_trade_fee;              //持卡人交易手续费
    private BigDecimal hand_fee;                        //应收费用
    private BigDecimal pay_fee;                         //应付费用
    private String error_reason;                        //差错原因
    private String out_identity_no;                     //接收机构标识码转出机构标识码
    private String out_card_no;                         //转出卡号
    private String in_identity_no;                      //转入机构标识码
    private String in_card_no;                          //转入卡号
    private String old_trade_time;                      //上一笔交易的日期时间
    private String card_serial_num;                     //卡片序列号
    private String terminal_reading_ability;            //终端读取能力
    private String iccard_condition_card;               //IC卡条件代码
    private String old_trade_clear_date;                //上一笔交易清算日期
    private BigDecimal old_trade_money;                 //上一笔交易金额
    private String trade_place_mark;                    //交易地域标志
    private String eci_sign;                            //ECI标志
    private String merchant_no;                         //商户代码
    private String send_clear_organ;                    //发送方清算机构
    private String send_receive_organ;                  //接收方清算机构转出方清算机构
    private String receive_clear_organ;                 //转入方清算机构
    private String old_trade_zdtype;                    //上一笔交易终端类型
    private String merchant_address;                    //商户名称地址
    private String special_fee_type;                    //特殊计费类型
    private String special_feedc;                       //特殊计费档次
    private String keepe;                               //保留使用
    private String card_sign_msg;                       //卡产品标识信息
    private String yfccjjdzysjjdjydm;                   //引发差错交易的最原始交易的交易代码
    private String trade_send_style;                    //交易发起方式
    private String merchan_clear_type;                  //账户结算类型
    private String order_id;                            //订单号
    private String keep_use;                            //保留使用
    private String belong_folder;                       //所属文件夹
    private Date analysis_time;                         //解析时间

    public String getError_trade_sign() {
        return error_trade_sign;
    }

    public void setError_trade_sign(String error_trade_sign) {
        this.error_trade_sign = error_trade_sign;
    }

    public String getProxy_code() {
        return proxy_code;
    }

    public void setProxy_code(String proxy_code) {
        this.proxy_code = proxy_code;
    }

    public String getSend_identifi_code() {
        return send_identifi_code;
    }

    public void setSend_identifi_code(String send_identifi_code) {
        this.send_identifi_code = send_identifi_code;
    }

    public String getSystem_track_num() {
        return system_track_num;
    }

    public void setSystem_track_num(String system_track_num) {
        this.system_track_num = system_track_num;
    }

    public Date getTrade_time() {
        return trade_time;
    }

    public void setTrade_time(Date trade_time) {
        this.trade_time = trade_time;
    }

    public String getBank_num() {
        return bank_num;
    }

    public void setBank_num(String bank_num) {
        this.bank_num = bank_num;
    }

    public BigDecimal getTrade_money() {
        return trade_money;
    }

    public void setTrade_money(BigDecimal trade_money) {
        this.trade_money = trade_money;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public String getTrade_message_no() {
        return trade_message_no;
    }

    public void setTrade_message_no(String trade_message_no) {
        this.trade_message_no = trade_message_no;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getTerminal_yard() {
        return terminal_yard;
    }

    public void setTerminal_yard(String terminal_yard) {
        this.terminal_yard = terminal_yard;
    }

    public String getOld_trade_retrieval_reference_num() {
        return old_trade_retrieval_reference_num;
    }

    public void setOld_trade_retrieval_reference_num(String old_trade_retrieval_reference_num) {
        this.old_trade_retrieval_reference_num = old_trade_retrieval_reference_num;
    }

    public String getService_point_condetion() {
        return service_point_condetion;
    }

    public void setService_point_condetion(String service_point_condetion) {
        this.service_point_condetion = service_point_condetion;
    }

    public String getAuthorization_response_code() {
        return authorization_response_code;
    }

    public void setAuthorization_response_code(String authorization_response_code) {
        this.authorization_response_code = authorization_response_code;
    }

    public String getAcceptance_mechanism() {
        return acceptance_mechanism;
    }

    public void setAcceptance_mechanism(String acceptance_mechanism) {
        this.acceptance_mechanism = acceptance_mechanism;
    }

    public String getBank_identity_no() {
        return bank_identity_no;
    }

    public void setBank_identity_no(String bank_identity_no) {
        this.bank_identity_no = bank_identity_no;
    }

    public String getOld_trade_system_track_num() {
        return old_trade_system_track_num;
    }

    public void setOld_trade_system_track_num(String old_trade_system_track_num) {
        this.old_trade_system_track_num = old_trade_system_track_num;
    }

    public String getTrade_return_code() {
        return trade_return_code;
    }

    public void setTrade_return_code(String trade_return_code) {
        this.trade_return_code = trade_return_code;
    }

    public String getService_input_style() {
        return service_input_style;
    }

    public void setService_input_style(String service_input_style) {
        this.service_input_style = service_input_style;
    }

    public BigDecimal getAgency_hand_fee() {
        return agency_hand_fee;
    }

    public void setAgency_hand_fee(BigDecimal agency_hand_fee) {
        this.agency_hand_fee = agency_hand_fee;
    }

    public BigDecimal getAgency_pay_fee() {
        return agency_pay_fee;
    }

    public void setAgency_pay_fee(BigDecimal agency_pay_fee) {
        this.agency_pay_fee = agency_pay_fee;
    }

    public BigDecimal getInstallment_payment_plus() {
        return installment_payment_plus;
    }

    public void setInstallment_payment_plus(BigDecimal installment_payment_plus) {
        this.installment_payment_plus = installment_payment_plus;
    }

    public BigDecimal getBank_per_trade_fee() {
        return bank_per_trade_fee;
    }

    public void setBank_per_trade_fee(BigDecimal bank_per_trade_fee) {
        this.bank_per_trade_fee = bank_per_trade_fee;
    }

    public BigDecimal getHand_fee() {
        return hand_fee;
    }

    public void setHand_fee(BigDecimal hand_fee) {
        this.hand_fee = hand_fee;
    }

    public BigDecimal getPay_fee() {
        return pay_fee;
    }

    public void setPay_fee(BigDecimal pay_fee) {
        this.pay_fee = pay_fee;
    }

    public String getError_reason() {
        return error_reason;
    }

    public void setError_reason(String error_reason) {
        this.error_reason = error_reason;
    }

    public String getOut_identity_no() {
        return out_identity_no;
    }

    public void setOut_identity_no(String out_identity_no) {
        this.out_identity_no = out_identity_no;
    }

    public String getOut_card_no() {
        return out_card_no;
    }

    public void setOut_card_no(String out_card_no) {
        this.out_card_no = out_card_no;
    }

    public String getIn_identity_no() {
        return in_identity_no;
    }

    public void setIn_identity_no(String in_identity_no) {
        this.in_identity_no = in_identity_no;
    }

    public String getIn_card_no() {
        return in_card_no;
    }

    public void setIn_card_no(String in_card_no) {
        this.in_card_no = in_card_no;
    }

    public String getOld_trade_time() {
        return old_trade_time;
    }

    public void setOld_trade_time(String old_trade_time) {
        this.old_trade_time = old_trade_time;
    }

    public String getCard_serial_num() {
        return card_serial_num;
    }

    public void setCard_serial_num(String card_serial_num) {
        this.card_serial_num = card_serial_num;
    }

    public String getTerminal_reading_ability() {
        return terminal_reading_ability;
    }

    public void setTerminal_reading_ability(String terminal_reading_ability) {
        this.terminal_reading_ability = terminal_reading_ability;
    }

    public String getIccard_condition_card() {
        return iccard_condition_card;
    }

    public void setIccard_condition_card(String iccard_condition_card) {
        this.iccard_condition_card = iccard_condition_card;
    }

    public String getOld_trade_clear_date() {
        return old_trade_clear_date;
    }

    public void setOld_trade_clear_date(String old_trade_clear_date) {
        this.old_trade_clear_date = old_trade_clear_date;
    }

    public BigDecimal getOld_trade_money() {
        return old_trade_money;
    }

    public void setOld_trade_money(BigDecimal old_trade_money) {
        this.old_trade_money = old_trade_money;
    }

    public String getTrade_place_mark() {
        return trade_place_mark;
    }

    public void setTrade_place_mark(String trade_place_mark) {
        this.trade_place_mark = trade_place_mark;
    }

    public String getEci_sign() {
        return eci_sign;
    }

    public void setEci_sign(String eci_sign) {
        this.eci_sign = eci_sign;
    }

    public String getMerchant_no() {
        return merchant_no;
    }

    public void setMerchant_no(String merchant_no) {
        this.merchant_no = merchant_no;
    }

    public String getSend_clear_organ() {
        return send_clear_organ;
    }

    public void setSend_clear_organ(String send_clear_organ) {
        this.send_clear_organ = send_clear_organ;
    }

    public String getSend_receive_organ() {
        return send_receive_organ;
    }

    public void setSend_receive_organ(String send_receive_organ) {
        this.send_receive_organ = send_receive_organ;
    }

    public String getReceive_clear_organ() {
        return receive_clear_organ;
    }

    public void setReceive_clear_organ(String receive_clear_organ) {
        this.receive_clear_organ = receive_clear_organ;
    }

    public String getOld_trade_zdtype() {
        return old_trade_zdtype;
    }

    public void setOld_trade_zdtype(String old_trade_zdtype) {
        this.old_trade_zdtype = old_trade_zdtype;
    }

    public String getMerchant_address() {
        return merchant_address;
    }

    public void setMerchant_address(String merchant_address) {
        this.merchant_address = merchant_address;
    }

    public String getSpecial_fee_type() {
        return special_fee_type;
    }

    public void setSpecial_fee_type(String special_fee_type) {
        this.special_fee_type = special_fee_type;
    }

    public String getSpecial_feedc() {
        return special_feedc;
    }

    public void setSpecial_feedc(String special_feedc) {
        this.special_feedc = special_feedc;
    }

    public String getKeepe() {
        return keepe;
    }

    public void setKeepe(String keepe) {
        this.keepe = keepe;
    }

    public String getCard_sign_msg() {
        return card_sign_msg;
    }

    public void setCard_sign_msg(String card_sign_msg) {
        this.card_sign_msg = card_sign_msg;
    }

    public String getYfccjjdzysjjdjydm() {
        return yfccjjdzysjjdjydm;
    }

    public void setYfccjjdzysjjdjydm(String yfccjjdzysjjdjydm) {
        this.yfccjjdzysjjdjydm = yfccjjdzysjjdjydm;
    }

    public String getTrade_send_style() {
        return trade_send_style;
    }

    public void setTrade_send_style(String trade_send_style) {
        this.trade_send_style = trade_send_style;
    }

    public String getMerchan_clear_type() {
        return merchan_clear_type;
    }

    public void setMerchan_clear_type(String merchan_clear_type) {
        this.merchan_clear_type = merchan_clear_type;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getKeep_use() {
        return keep_use;
    }

    public void setKeep_use(String keep_use) {
        this.keep_use = keep_use;
    }

    public String getBelong_folder() {
        return belong_folder;
    }

    public void setBelong_folder(String belong_folder) {
        this.belong_folder = belong_folder;
    }

    public Date getAnalysis_time() {
        return analysis_time;
    }

    public void setAnalysis_time(Date analysis_time) {
        this.analysis_time = analysis_time;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
