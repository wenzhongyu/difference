package com.mt.error.dal.bsp.entity;

import com.alibaba.fastjson.JSONObject;
import com.mt.error.dal.common.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wenzhong on 2017/1/4.
 */
@Entity
@Table(name = "details_uniopay_acom")
public class DetailsUnionPayAcomDO extends BaseEntity {
   private String proxy_code;                       //代理机构标识码
   private String send_identifi_code;               //发送机构标识码
   private String system_track_num;	                //系统跟踪号
   private Date union_pay_tadetime;                 //交易传输时间
   private String bank_num;                         //主帐号
   private BigDecimal trade_money;                  //交易金额
   private BigDecimal collection_against_money;     //部分代收时的承兑金额
   private BigDecimal cardholder_fee;               //持卡人交易手续费
   private String message_type;                     //报文类型
   private String trade_message_no;                 //交易类型码
   private String account_type;                     //商户类型
   private String terminal_yard;                    //受卡机终端标识码
   private String by_square_identi_card;            //受卡方标识码
   private String retrieval_reference_num;          //检索参考号
   private String service_point_condetion;          //服务点条件码
   private String authorization_response_code;      //授权应答码
   private String acceptance_mechanism;             //接收机构标识码
   private String old_system_track_num;             //原始交易的系统跟踪号
   private String trade_return_code;                //交易返回码
   private String service_input_style;              //服务点输入方式
   private BigDecimal agency_hand_fee;              //受理方应收手续费
   private BigDecimal agency_pay_fee;               //受理方应付手续费
   private BigDecimal trans_service_charge;         //转接服务费
   private String sd_conver_mark;                   //单双转换标志
   private String card_serial_num;                  //卡片序列号
   private String terminal_reading_ability;         //终端读取能力
   private String ic_card_condition_card;           //IC卡条件代码
   private String old_trade_time;                   //原始交易日期时间
   private String card_identification_code;         //发卡机构标识码
   private String trade_place_sign;                 //交易地域标志
   private String trerminal_type;                   //终端类型
   private Date analysis_time;                      //解析时间
   private String belong_folder;                    //所属文件夹
   private String eci_sign;                         //ECI标志
   private String installment_payment_plus;         //分期付款附加手续费
   private String other_message;                    //其它信息
   private String non_standard;                     //非标价格商户
   private String card_level;                       //卡账户登记
   private String card_product;                     //卡产品
   private String is_standard;                      //是否银联标准卡
   private String keep_use;                         //保留使用
   private String nKeep_use;                        //保留使用
   private String stage_number;                     //分期付款期数
   private String order_id;                         //订单号
   private String pay_style;                        //支付方式
   private String nkeep_use2;                       //保留使用

    public String getProxy_code() {
        return proxy_code;
    }

    public void setProxy_code(String proxy_code) {
        this.proxy_code = proxy_code;
    }

    public String getSend_identifi_code() {
        return send_identifi_code;
    }

    public void setSend_identifi_code(String send_identifi_code) {
        this.send_identifi_code = send_identifi_code;
    }

    public String getSystem_track_num() {
        return system_track_num;
    }

    public void setSystem_track_num(String system_track_num) {
        this.system_track_num = system_track_num;
    }

    public Date getUnion_pay_tadetime() {
        return union_pay_tadetime;
    }

    public void setUnion_pay_tadetime(Date union_pay_tadetime) {
        this.union_pay_tadetime = union_pay_tadetime;
    }

    public String getBank_num() {
        return bank_num;
    }

    public void setBank_num(String bank_num) {
        this.bank_num = bank_num;
    }

    public BigDecimal getTrade_money() {
        return trade_money;
    }

    public void setTrade_money(BigDecimal trade_money) {
        this.trade_money = trade_money;
    }

    public BigDecimal getCollection_against_money() {
        return collection_against_money;
    }

    public void setCollection_against_money(BigDecimal collection_against_money) {
        this.collection_against_money = collection_against_money;
    }

    public BigDecimal getCardholder_fee() {
        return cardholder_fee;
    }

    public void setCardholder_fee(BigDecimal cardholder_fee) {
        this.cardholder_fee = cardholder_fee;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public String getTrade_message_no() {
        return trade_message_no;
    }

    public void setTrade_message_no(String trade_message_no) {
        this.trade_message_no = trade_message_no;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getTerminal_yard() {
        return terminal_yard;
    }

    public void setTerminal_yard(String terminal_yard) {
        this.terminal_yard = terminal_yard;
    }

    public String getBy_square_identi_card() {
        return by_square_identi_card;
    }

    public void setBy_square_identi_card(String by_square_identi_card) {
        this.by_square_identi_card = by_square_identi_card;
    }

    public String getRetrieval_reference_num() {
        return retrieval_reference_num;
    }

    public void setRetrieval_reference_num(String retrieval_reference_num) {
        this.retrieval_reference_num = retrieval_reference_num;
    }

    public String getService_point_condetion() {
        return service_point_condetion;
    }

    public void setService_point_condetion(String service_point_condetion) {
        this.service_point_condetion = service_point_condetion;
    }

    public String getAuthorization_response_code() {
        return authorization_response_code;
    }

    public void setAuthorization_response_code(String authorization_response_code) {
        this.authorization_response_code = authorization_response_code;
    }

    public String getAcceptance_mechanism() {
        return acceptance_mechanism;
    }

    public void setAcceptance_mechanism(String acceptance_mechanism) {
        this.acceptance_mechanism = acceptance_mechanism;
    }

    public String getOld_system_track_num() {
        return old_system_track_num;
    }

    public void setOld_system_track_num(String old_system_track_num) {
        this.old_system_track_num = old_system_track_num;
    }

    public String getTrade_return_code() {
        return trade_return_code;
    }

    public void setTrade_return_code(String trade_return_code) {
        this.trade_return_code = trade_return_code;
    }

    public String getService_input_style() {
        return service_input_style;
    }

    public void setService_input_style(String service_input_style) {
        this.service_input_style = service_input_style;
    }

    public BigDecimal getAgency_hand_fee() {
        return agency_hand_fee;
    }

    public void setAgency_hand_fee(BigDecimal agency_hand_fee) {
        this.agency_hand_fee = agency_hand_fee;
    }

    public BigDecimal getAgency_pay_fee() {
        return agency_pay_fee;
    }

    public void setAgency_pay_fee(BigDecimal agency_pay_fee) {
        this.agency_pay_fee = agency_pay_fee;
    }

    public BigDecimal getTrans_service_charge() {
        return trans_service_charge;
    }

    public void setTrans_service_charge(BigDecimal trans_service_charge) {
        this.trans_service_charge = trans_service_charge;
    }

    public String getSd_conver_mark() {
        return sd_conver_mark;
    }

    public void setSd_conver_mark(String sd_conver_mark) {
        this.sd_conver_mark = sd_conver_mark;
    }

    public String getCard_serial_num() {
        return card_serial_num;
    }

    public void setCard_serial_num(String card_serial_num) {
        this.card_serial_num = card_serial_num;
    }

    public String getTerminal_reading_ability() {
        return terminal_reading_ability;
    }

    public void setTerminal_reading_ability(String terminal_reading_ability) {
        this.terminal_reading_ability = terminal_reading_ability;
    }

    public String getIc_card_condition_card() {
        return ic_card_condition_card;
    }

    public void setIc_card_condition_card(String ic_card_condition_card) {
        this.ic_card_condition_card = ic_card_condition_card;
    }

    public String getOld_trade_time() {
        return old_trade_time;
    }

    public void setOld_trade_time(String old_trade_time) {
        this.old_trade_time = old_trade_time;
    }

    public String getCard_identification_code() {
        return card_identification_code;
    }

    public void setCard_identification_code(String card_identification_code) {
        this.card_identification_code = card_identification_code;
    }

    public String getTrade_place_sign() {
        return trade_place_sign;
    }

    public void setTrade_place_sign(String trade_place_sign) {
        this.trade_place_sign = trade_place_sign;
    }

    public String getTrerminal_type() {
        return trerminal_type;
    }

    public void setTrerminal_type(String trerminal_type) {
        this.trerminal_type = trerminal_type;
    }

    public Date getAnalysis_time() {
        return analysis_time;
    }

    public void setAnalysis_time(Date analysis_time) {
        this.analysis_time = analysis_time;
    }

    public String getBelong_folder() {
        return belong_folder;
    }

    public void setBelong_folder(String belong_folder) {
        this.belong_folder = belong_folder;
    }

    public String getEci_sign() {
        return eci_sign;
    }

    public void setEci_sign(String eci_sign) {
        this.eci_sign = eci_sign;
    }

    public String getInstallment_payment_plus() {
        return installment_payment_plus;
    }

    public void setInstallment_payment_plus(String installment_payment_plus) {
        this.installment_payment_plus = installment_payment_plus;
    }

    public String getOther_message() {
        return other_message;
    }

    public void setOther_message(String other_message) {
        this.other_message = other_message;
    }

    public String getNon_standard() {
        return non_standard;
    }

    public void setNon_standard(String non_standard) {
        this.non_standard = non_standard;
    }

    public String getCard_level() {
        return card_level;
    }

    public void setCard_level(String card_level) {
        this.card_level = card_level;
    }

    public String getCard_product() {
        return card_product;
    }

    public void setCard_product(String card_product) {
        this.card_product = card_product;
    }

    public String getIs_standard() {
        return is_standard;
    }

    public void setIs_standard(String is_standard) {
        this.is_standard = is_standard;
    }

    public String getKeep_use() {
        return keep_use;
    }

    public void setKeep_use(String keep_use) {
        this.keep_use = keep_use;
    }

    public String getnKeep_use() {
        return nKeep_use;
    }

    public void setnKeep_use(String nKeep_use) {
        this.nKeep_use = nKeep_use;
    }

    public String getStage_number() {
        return stage_number;
    }

    public void setStage_number(String stage_number) {
        this.stage_number = stage_number;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPay_style() {
        return pay_style;
    }

    public void setPay_style(String pay_style) {
        this.pay_style = pay_style;
    }

    public String getNkeep_use2() {
        return nkeep_use2;
    }

    public void setNkeep_use2(String nkeep_use2) {
        this.nkeep_use2 = nkeep_use2;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
