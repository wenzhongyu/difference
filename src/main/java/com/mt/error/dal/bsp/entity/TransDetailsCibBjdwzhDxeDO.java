package com.mt.error.dal.bsp.entity;

import com.alibaba.fastjson.JSONObject;
import com.mt.error.dal.common.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wenzhong on 2017/1/4.
 */
@Entity
@Table(name = "trans_details_cib_bjdwzh_dxe")
public class TransDetailsCibBjdwzhDxeDO extends BaseEntity{
    private String ad;				//AD
    private String payno;				//付款账号
    private String clerk_order_id;				//柜员流水号
    private String trade_type;				//交易类型
    private String abstract_code;				//核心摘要代码
    private Date bill_date;				//记账日期
    private BigDecimal trade_money;				//交易金额
    private BigDecimal balance;				//余额
    private String currency;				//币种
    private String abstract_short;				//摘要简称
    private String other_no;				//对方账号
    private String other_name;				//对方账户名称
    private String self_proffno;				//本行凭证代号
    private String other_proff_type;				//他行凭证类型
    private String other_proffno;				//他行凭证号码
    private String add_name;				//附加行名
    private String add_num;				//附加行号
    private String biz_type;				//业务类型
    private String flow_order_id;				//流水唯一标识号
    private String bank_name;				//银行名称
    private Date analysis_time;				//解析时间

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getPayno() {
        return payno;
    }

    public void setPayno(String payno) {
        this.payno = payno;
    }

    public String getClerk_order_id() {
        return clerk_order_id;
    }

    public void setClerk_order_id(String clerk_order_id) {
        this.clerk_order_id = clerk_order_id;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getAbstract_code() {
        return abstract_code;
    }

    public void setAbstract_code(String abstract_code) {
        this.abstract_code = abstract_code;
    }

    public Date getBill_date() {
        return bill_date;
    }

    public void setBill_date(Date bill_date) {
        this.bill_date = bill_date;
    }

    public BigDecimal getTrade_money() {
        return trade_money;
    }

    public void setTrade_money(BigDecimal trade_money) {
        this.trade_money = trade_money;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAbstract_short() {
        return abstract_short;
    }

    public void setAbstract_short(String abstract_short) {
        this.abstract_short = abstract_short;
    }

    public String getOther_no() {
        return other_no;
    }

    public void setOther_no(String other_no) {
        this.other_no = other_no;
    }

    public String getOther_name() {
        return other_name;
    }

    public void setOther_name(String other_name) {
        this.other_name = other_name;
    }

    public String getSelf_proffno() {
        return self_proffno;
    }

    public void setSelf_proffno(String self_proffno) {
        this.self_proffno = self_proffno;
    }

    public String getOther_proff_type() {
        return other_proff_type;
    }

    public void setOther_proff_type(String other_proff_type) {
        this.other_proff_type = other_proff_type;
    }

    public String getOther_proffno() {
        return other_proffno;
    }

    public void setOther_proffno(String other_proffno) {
        this.other_proffno = other_proffno;
    }

    public String getAdd_name() {
        return add_name;
    }

    public void setAdd_name(String add_name) {
        this.add_name = add_name;
    }

    public String getAdd_num() {
        return add_num;
    }

    public void setAdd_num(String add_num) {
        this.add_num = add_num;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getFlow_order_id() {
        return flow_order_id;
    }

    public void setFlow_order_id(String flow_order_id) {
        this.flow_order_id = flow_order_id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public Date getAnalysis_time() {
        return analysis_time;
    }

    public void setAnalysis_time(Date analysis_time) {
        this.analysis_time = analysis_time;
    }

    @Override
    public String toString(){
        return JSONObject.toJSONString(this);
    }
}
