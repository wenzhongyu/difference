package com.mt.error.dal.bsp.mapper;

import com.mt.error.dal.bsp.entity.DifOrderDO;
import com.mt.error.dal.common.mapper.BaseMapper;

/**
 * Created by wenzhong on 2017/1/4.
 */
public interface DifOrderMapper extends BaseMapper<DifOrderDO, Long> {
}
