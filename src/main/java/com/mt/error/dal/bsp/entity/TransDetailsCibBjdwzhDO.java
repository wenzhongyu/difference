package com.mt.error.dal.bsp.entity;

import com.alibaba.fastjson.JSONObject;
import com.mt.error.dal.common.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wenzhong on 2017/1/4.
 */
@Entity
@Table(name = "trans_details_cib_bjdwzh")
public class TransDetailsCibBjdwzhDO extends BaseEntity {
    private Date trans_date; 				//订单交易日期
    private String sno; 				//订单流水号
    private String mrch_no; 				//商户号
    private String order_no; 				//订单编号
    private String mask_acc_no; 				//屏蔽的银行帐号
    private BigDecimal trans_amt; 				//支付金额
    private BigDecimal return_amt; 				//退款金额
    private BigDecimal trans_fee; 				//手续费
    private String order_state; 				//订单状态
    private Date trans_time; 				//订单交易发生时间
    private String settle_date; 				//资金结算日期
    private Date analysis_time; 				//解析时间
    private Date insert_time; 				//插入时间

    public Date getTrans_date() {
        return trans_date;
    }

    public void setTrans_date(Date trans_date) {
        this.trans_date = trans_date;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getMrch_no() {
        return mrch_no;
    }

    public void setMrch_no(String mrch_no) {
        this.mrch_no = mrch_no;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getMask_acc_no() {
        return mask_acc_no;
    }

    public void setMask_acc_no(String mask_acc_no) {
        this.mask_acc_no = mask_acc_no;
    }

    public BigDecimal getTrans_amt() {
        return trans_amt;
    }

    public void setTrans_amt(BigDecimal trans_amt) {
        this.trans_amt = trans_amt;
    }

    public BigDecimal getReturn_amt() {
        return return_amt;
    }

    public void setReturn_amt(BigDecimal return_amt) {
        this.return_amt = return_amt;
    }

    public BigDecimal getTrans_fee() {
        return trans_fee;
    }

    public void setTrans_fee(BigDecimal trans_fee) {
        this.trans_fee = trans_fee;
    }

    public String getOrder_state() {
        return order_state;
    }

    public void setOrder_state(String order_state) {
        this.order_state = order_state;
    }

    public Date getTrans_time() {
        return trans_time;
    }

    public void setTrans_time(Date trans_time) {
        this.trans_time = trans_time;
    }

    public String getSettle_date() {
        return settle_date;
    }

    public void setSettle_date(String settle_date) {
        this.settle_date = settle_date;
    }

    public Date getAnalysis_time() {
        return analysis_time;
    }

    public void setAnalysis_time(Date analysis_time) {
        this.analysis_time = analysis_time;
    }

    public Date getInsert_time() {
        return insert_time;
    }

    public void setInsert_time(Date insert_time) {
        this.insert_time = insert_time;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
