package com.mt.error.dal.datacenter.mapper;

import com.mt.error.dal.common.mapper.BaseMapper;
import com.mt.error.dal.datacenter.entity.AccountTradeSuboneDO;

/**
 * Created by wenzhong on 2017/1/3.
 */
public interface AccountTradeSuboneMapper extends BaseMapper<AccountTradeSuboneDO, Integer> {
}
