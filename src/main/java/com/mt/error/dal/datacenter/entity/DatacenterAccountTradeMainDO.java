package com.mt.error.dal.datacenter.entity;

import com.alibaba.fastjson.JSONObject;
import com.mt.error.dal.common.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wenzhong on 2017/1/4.
 */
@Entity
@Table(name = "account_trade_main ")
public class DatacenterAccountTradeMainDO extends BaseEntity {

    private Date pay_finishtime;                //交易完成时间
    private String finance_orderid;             //财务流水号
    private Long order_id;                      //订单号
    private String total_txntype;               //交易大类
    private Integer txn_type;                   //交易类型
    private Long user_no;                       //用户编号
    private BigDecimal pay_money;               //交易金额
    private BigDecimal ought_fee;               //应收手续费
    private BigDecimal deduction_fee;           //抵扣手续费
    private BigDecimal real_fee;                //实收手续费
    private BigDecimal profit_fee;              //分润
    private BigDecimal minimum_fee;             //机构保底手续费
    private String deal_channel;                //处理渠道
    private Date settle_date;                   //清算日期
    private String bank_orderid;                //银行订单号
    private String send_idcode;                 //发送机构标识码
    private String rec_idcode;                  //接收机构标识码
    private String trans_time;                  //交易传输时间
    private String ref_num;                     //检索参考号
    private String merchantno;                  //商户编号
    private Boolean suc_flag;                   //成功标识
    private Boolean cancel_flag;                //撤销冲正退货标识
    private BigDecimal return_goodsmoney;       //退货金额
    private Date update_time;                   //更新时间
    private Date insert_time;                   //插入时间
    private Integer ckflag;                     //核对标识
    private Integer clear_flag;                 //清算标识
    private Integer clear_batchno;              //清算批次号
    private Boolean settle_flag;                //结算标识
    private String settle_batchno;              //结算批次号
    private String errorSign;                   //差错交易标识

    public Date getPay_finishtime() {
        return pay_finishtime;
    }

    public void setPay_finishtime(Date pay_finishtime) {
        this.pay_finishtime = pay_finishtime;
    }

    public String getFinance_orderid() {
        return finance_orderid;
    }

    public void setFinance_orderid(String finance_orderid) {
        this.finance_orderid = finance_orderid;
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    public String getTotal_txntype() {
        return total_txntype;
    }

    public void setTotal_txntype(String total_txntype) {
        this.total_txntype = total_txntype;
    }

    public Integer getTxn_type() {
        return txn_type;
    }

    public void setTxn_type(Integer txn_type) {
        this.txn_type = txn_type;
    }

    public Long getUser_no() {
        return user_no;
    }

    public void setUser_no(Long user_no) {
        this.user_no = user_no;
    }

    public BigDecimal getPay_money() {
        return pay_money;
    }

    public void setPay_money(BigDecimal pay_money) {
        this.pay_money = pay_money;
    }

    public BigDecimal getOught_fee() {
        return ought_fee;
    }

    public void setOught_fee(BigDecimal ought_fee) {
        this.ought_fee = ought_fee;
    }

    public BigDecimal getDeduction_fee() {
        return deduction_fee;
    }

    public void setDeduction_fee(BigDecimal deduction_fee) {
        this.deduction_fee = deduction_fee;
    }

    public BigDecimal getReal_fee() {
        return real_fee;
    }

    public void setReal_fee(BigDecimal real_fee) {
        this.real_fee = real_fee;
    }

    public BigDecimal getProfit_fee() {
        return profit_fee;
    }

    public void setProfit_fee(BigDecimal profit_fee) {
        this.profit_fee = profit_fee;
    }

    public BigDecimal getMinimum_fee() {
        return minimum_fee;
    }

    public void setMinimum_fee(BigDecimal minimum_fee) {
        this.minimum_fee = minimum_fee;
    }

    public String getDeal_channel() {
        return deal_channel;
    }

    public void setDeal_channel(String deal_channel) {
        this.deal_channel = deal_channel;
    }

    public Date getSettle_date() {
        return settle_date;
    }

    public void setSettle_date(Date settle_date) {
        this.settle_date = settle_date;
    }

    public String getBank_orderid() {
        return bank_orderid;
    }

    public void setBank_orderid(String bank_orderid) {
        this.bank_orderid = bank_orderid;
    }

    public String getSend_idcode() {
        return send_idcode;
    }

    public void setSend_idcode(String send_idcode) {
        this.send_idcode = send_idcode;
    }

    public String getRec_idcode() {
        return rec_idcode;
    }

    public void setRec_idcode(String rec_idcode) {
        this.rec_idcode = rec_idcode;
    }

    public String getTrans_time() {
        return trans_time;
    }

    public void setTrans_time(String trans_time) {
        this.trans_time = trans_time;
    }

    public String getRef_num() {
        return ref_num;
    }

    public void setRef_num(String ref_num) {
        this.ref_num = ref_num;
    }

    public String getMerchantno() {
        return merchantno;
    }

    public void setMerchantno(String merchantno) {
        this.merchantno = merchantno;
    }

    public Boolean getSuc_flag() {
        return suc_flag;
    }

    public void setSuc_flag(Boolean suc_flag) {
        this.suc_flag = suc_flag;
    }

    public Boolean getCancel_flag() {
        return cancel_flag;
    }

    public void setCancel_flag(Boolean cancel_flag) {
        this.cancel_flag = cancel_flag;
    }

    public BigDecimal getReturn_goodsmoney() {
        return return_goodsmoney;
    }

    public void setReturn_goodsmoney(BigDecimal return_goodsmoney) {
        this.return_goodsmoney = return_goodsmoney;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public Date getInsert_time() {
        return insert_time;
    }

    public void setInsert_time(Date insert_time) {
        this.insert_time = insert_time;
    }

    public Integer getCkflag() {
        return ckflag;
    }

    public void setCkflag(Integer ckflag) {
        this.ckflag = ckflag;
    }

    public Integer getClear_flag() {
        return clear_flag;
    }

    public void setClear_flag(Integer clear_flag) {
        this.clear_flag = clear_flag;
    }

    public Integer getClear_batchno() {
        return clear_batchno;
    }

    public void setClear_batchno(Integer clear_batchno) {
        this.clear_batchno = clear_batchno;
    }

    public Boolean getSettle_flag() {
        return settle_flag;
    }

    public void setSettle_flag(Boolean settle_flag) {
        this.settle_flag = settle_flag;
    }

    public String getSettle_batchno() {
        return settle_batchno;
    }

    public void setSettle_batchno(String settle_batchno) {
        this.settle_batchno = settle_batchno;
    }

    public String getErrorSign() {
        return errorSign;
    }

    public void setErrorSign(String errorSign) {
        this.errorSign = errorSign;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
