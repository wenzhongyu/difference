package com.mt.error.dal.datacenter.mapper;

import com.mt.error.dal.common.mapper.BaseMapper;
import com.mt.error.dal.datacenter.entity.DatacenterAccountTradeMainDO;

/**
 * Created by wenzhong on 2017/1/4.
 */
public interface DatacenterAccountTradeMainMapper extends BaseMapper<DatacenterAccountTradeMainDO, Long> {
}
