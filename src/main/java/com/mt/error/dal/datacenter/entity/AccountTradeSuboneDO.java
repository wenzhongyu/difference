package com.mt.error.dal.datacenter.entity;

import com.alibaba.fastjson.JSONObject;
import com.mt.error.dal.common.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Created by wenzhong on 2017/1/4.
 */
@Entity
@Table(name = "account_trade_subone")
public class AccountTradeSuboneDO extends BaseEntity {

    private Long order_id;							    //订单号
    private BigDecimal balance;							//余额
    private BigDecimal balance_freeze;					//余额冻结额
    private BigDecimal unsettled_freeze;				//未结算冻结额
    private BigDecimal balance_change;					//余额变动额
    private BigDecimal balance_freeze_change;			//余额冻结变动额
    private BigDecimal unsettled_freeze_change;			//未结算冻结变动额
    private String ret_code;							//返回码
    private String ret_msg;							    //返回信息
    private String card_no;							    //卡号
    private String bank_name;							//开户行名称
    private Integer card_type;							//卡类型
    private Integer iccard_flag;						//ic卡标识
    private String mcc;							        //MCC行业分类
    private String input_type;							//输入类型
    private String province;							//交易省份
    private String city;							    //交易城市
    private String county;							    //交易县（区）
    private String device_no;							//设备编号
    private String longitude;							//经度
    private String latitude;							//纬度
    private String terminalno;							//终端编号

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance_freeze() {
        return balance_freeze;
    }

    public void setBalance_freeze(BigDecimal balance_freeze) {
        this.balance_freeze = balance_freeze;
    }

    public BigDecimal getUnsettled_freeze() {
        return unsettled_freeze;
    }

    public void setUnsettled_freeze(BigDecimal unsettled_freeze) {
        this.unsettled_freeze = unsettled_freeze;
    }

    public BigDecimal getBalance_change() {
        return balance_change;
    }

    public void setBalance_change(BigDecimal balance_change) {
        this.balance_change = balance_change;
    }

    public BigDecimal getBalance_freeze_change() {
        return balance_freeze_change;
    }

    public void setBalance_freeze_change(BigDecimal balance_freeze_change) {
        this.balance_freeze_change = balance_freeze_change;
    }

    public BigDecimal getUnsettled_freeze_change() {
        return unsettled_freeze_change;
    }

    public void setUnsettled_freeze_change(BigDecimal unsettled_freeze_change) {
        this.unsettled_freeze_change = unsettled_freeze_change;
    }

    public String getRet_code() {
        return ret_code;
    }

    public void setRet_code(String ret_code) {
        this.ret_code = ret_code;
    }

    public String getRet_msg() {
        return ret_msg;
    }

    public void setRet_msg(String ret_msg) {
        this.ret_msg = ret_msg;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public Integer getCard_type() {
        return card_type;
    }

    public void setCard_type(Integer card_type) {
        this.card_type = card_type;
    }

    public Integer getIccard_flag() {
        return iccard_flag;
    }

    public void setIccard_flag(Integer iccard_flag) {
        this.iccard_flag = iccard_flag;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getInput_type() {
        return input_type;
    }

    public void setInput_type(String input_type) {
        this.input_type = input_type;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getDevice_no() {
        return device_no;
    }

    public void setDevice_no(String device_no) {
        this.device_no = device_no;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTerminalno() {
        return terminalno;
    }

    public void setTerminalno(String terminalno) {
        this.terminalno = terminalno;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
