module.exports = {
    routers: [
	    {
	        path: '/trade-difference/list',
	        component: require("../module/list-trade-difference-module")
	    },
	    {
	        path: '/trade-difference/add',
	        component: require("../module/add-trade-difference-module")
	    },
	    {
	        path: '/unionpay-error/list',
	        component: require("../module/list-unionpay-error-module")
	    }
    ]
};