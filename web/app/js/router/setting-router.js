module.exports = {
    routers: [
	    {
	        path: '/difference-type/list',
	        component: require("../module/list-difference-type-module")
	    },
	    {
	        path: '/difference-type/add',
	        component: require("../module/add-difference-type-module")
	    },
	    {
	        path: '/difference-type/edit',
	        component: require("../module/edit-difference-type-module")
	    }
    ]
};