require('../scss/app.scss');

import Vue from 'vue';
import VueRouter from 'vue-router';
import ElementUI from 'element-ui'

Vue.use(VueRouter);
Vue.use(ElementUI);

Vue.component('qiandai-header', Vue.extend(require("./component/header")));
Vue.component('qiandai-footer', Vue.extend(require("./component/footer")));
Vue.component('qiandai-sidebar', Vue.extend(require("./component/sidebar")));

var routes = [];
routes = routes.concat(require("./router/business-router").routers);
routes = routes.concat(require("./router/setting-router").routers);

var router = new VueRouter({
    routes
});

var app = new Vue({
    router
}).$mount('#app');
