module.exports = {
    template: require('../../template/list-unionpay-error.html'),
    data: function(){
    	var data = [];
    	for (var i=0; i<10; i++) {
    		data.push({
    			id: i,
    			tradeMoney: "1000.00",
    			oldTradeMoney: "999.00",
    			pay_fee: "50.00",
    			error_trade_sign: "E23",
    			error_reason: "4502",
    			system_track_num: "963405",
    			bank_num: "4380886956576957"
    		});
    	}
		return {
			unionPayErrorList: data,
			total: 50,
			pageSize: 10,
			multipleSelection: []
		};
	},
	methods: {
		handleSelectionChange(val) {
			this.multipleSelection = val;
		},
		handleSizeChange(val) {
			console.log(`每页 ${val} 条`);
		},
		handleCurrentChange(val) {
			this.currentPage = val;
			console.log(`当前页: ${val}`);
		}
	}
};