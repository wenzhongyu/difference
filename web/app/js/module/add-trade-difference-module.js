module.exports = {
    template: require('../../template/add-trade-difference.html'),
    data: function(){
        var data = [];
        for (var i=0; i<5; i++) {
            data.push({
                id: i,
                bank_name: "中国建设银行",
                in_orout: "出金",
                system_orderid: "123456789012345678901234567",
                ref_num: "9876543210",
                paymoney: "1200000.00",
                deal_channel: "银联总部POS"
            });
        }
        return {
            tradeList: data,
            total: 50,
            pageSize: 10,
            currentRow: null
        };
    },
    mounted: function() {
        this.currentRow = this.tradeList[0];

        document.getElementById("trade-table").getElementsByTagName("tbody")[0].children[0].className="current-row";
    },
    methods: {
        handleCurrentChange(val) {
            this.currentPage = val;
            console.log(this.currentPage);
        },
        select(val) {
            this.currentRow = val;

            console.log(this.$refs.table);
        }
    }
};