module.exports = {
    template: require('../../template/list-difference-type.html'),
    data: function(){
    	var data = [];
    	for (var i=0; i<10; i++) {
    		data.push({
    			id: i,
    			code: "0BH01",
    			codeDec: "出款失败后银行扣款后又返回",
    			isMixFee: "是"
    		});
    	}
		return {
			differenceTypeList: data,
			total: 50,
			pageSize: 10
		};
	},
	methods: {
		handleSelectionChange(val) {
			this.multipleSelection = val;
		},
		handleSizeChange(val) {
			console.log(`每页 ${val} 条`);
		},
		handleCurrentChange(val) {
			this.currentPage = val;
			console.log(`当前页: ${val}`);
		}
	}
};