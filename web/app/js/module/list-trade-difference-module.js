module.exports = {
    template: require('../../template/list-trade-difference.html'),
    data: function(){
    	var data = [];
    	for (var i=0; i<10; i++) {
    		data.push({
    			id: i,
    			bank_name: "中国建设银行",
    			diftype: "系统对账-银行",
    			in_orout: "出金",
    			system_orderid: "123456789012345678901234567",
    			ref_num: "9876543210",
    			paymoney: "1200000.00",
    			deal_channel: "银联总部POS",
    			error_dec: "系统记录成功，但银行记录失败（无清算的记录）或银行记录金额小于实际交易金额（短款）",
    			mixtype: "贷记调整",
    			tzstate: "处理中"
    		});
    	}
		return {
			tradeDifferenceList: data,
            total: 50,
            pageSize: 10,
			multipleSelection: []
		};
	},
	methods: {
		handleSelectionChange(val) {
			this.multipleSelection = val;

            console.log(this.multipleSelection);
		},
		handleSizeChange(val) {
			this.pageSize = val;

            console.log(this.pageSize);
		},
		handleCurrentChange(val) {
			this.currentPage = val;

			console.log(this.currentPage);
		}
	}
};